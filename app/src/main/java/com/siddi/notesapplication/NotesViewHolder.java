package com.siddi.notesapplication;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

public class NotesViewHolder extends RecyclerView.ViewHolder {

    public TextView note_title;
    public TextView last_updated;
    public TextView note_text;


    public NotesViewHolder(View view) {
        super(view);
        note_title = (TextView) view.findViewById(R.id.title);
        last_updated = (TextView) view.findViewById(R.id.date_modified);
        note_text = (TextView) view.findViewById(R.id.note_text);
    }

}
