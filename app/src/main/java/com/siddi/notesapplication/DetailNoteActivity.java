package com.siddi.notesapplication;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


public class DetailNoteActivity extends AppCompatActivity {

    private TextView noteTitle;
    private TextView noteText;
    private TextView noteDate;

    private Notes note = new Notes();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        noteTitle = findViewById(R.id.noteTitle);
        noteText = findViewById(R.id.noteText);
        noteDate = findViewById(R.id.noteDate);

        Intent intent = getIntent();

        if (intent.hasExtra("Notes_Object")) {
            note = (Notes) intent.getSerializableExtra("Notes_Object");
            noteTitle.setText(note.getNoteTitle());
            noteText.setText(note.getNoteText());
            noteDate.setText(note.getLastUpdated());
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
